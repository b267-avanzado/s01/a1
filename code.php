<?php

function getFullAddress($country, $city, $province, $houseNumber) {
    return "$houseNumber, $city, $province, $country";
}

function getLetterGrade($grade) {
    if ($grade <=100 && $grade >=98) {
        return "$grade is equivalent to A+";
    } else if ($grade <=97 && $grade >=95) {
        return "$grade is equivalent to A";
    } else if ($grade <=94 && $grade >=92) {
        return "$grade is equivalent to A-";
    } else if ($grade <=91 && $grade >=89) {
        return "$grade is equivalent to B+";
    } else if ($grade <=88 && $grade >=86) {
        return "$grade is equivalent to B";
    } else if ($grade <=85 && $grade >=83) {
        return "$grade is equivalent to B-";
    } else if ($grade <=82 && $grade >=80) {
        return "$grade is equivalent to C+";
    } else if ($grade <=79 && $grade >=77) {
        return "$grade is equivalent to C";
    } else if ($grade <=76 && $grade >=75) {
        return "$grade is equivalent to C-";
    } else if ($grade <=74 && $grade >0) {
        return "$grade is equivalent to F";
    }
}