<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S01 - Activity</title>
    </head>
    <body>
        <h1>Full Address</h1>
        <p><?php echo getFullAddress( "Philippines", "Quezon City", "Metro Manila", 123) ?></p>

        <h1>Letter-Based Grading</h1>
        <p><?php echo getLetterGrade(87) ?></p>
    </body>
</html>